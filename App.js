/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Fragment, useRef} from 'react';
import {Linking} from 'react-native';
import {StyleSheet, View} from 'react-native';
import HTML, {IGNORED_TAGS} from 'react-native-render-html';
import Video from 'react-native-video';
import Logo from './src/components/Logo';
import Share from './src/components/Share';

import {getLanguage, mapLanguages} from './src/modules';
import {useQuote} from './src/modules/quotes';

const App: () => React$Node = () => {
  const {quote} = useQuote();
  const deviceLanguage = getLanguage();
  const shotRef = useRef(null);

  return (
    <View style={styles.outerContainer}>
      <Logo />
      <View style={styles.container}>
        {quote && quote.en && (
          <Fragment>
            <HTML
              source={{html: mapLanguages(quote)[deviceLanguage]}}
              onLinkPress={(event, linkUrl) => {
                Linking.openURL(linkUrl);
              }}
              classesStyles={styles}
              ignoredTags={IGNORED_TAGS.filter((tag) => tag !== 'video')}
              renderers={{
                video: (
                  htmlAttribs,
                  children,
                  convertedCSSStyles,
                  passProps,
                ) => {
                  return (
                    <Video
                      key="video_key"
                      source={{uri: htmlAttribs.src}}
                      ref={(ref) => {
                        this.player = ref;
                      }}
                      //onBuffer={this.onBuffer}
                      //onError={this.videoError}
                      style={styles.backgroundVideo}
                    />
                  );
                },
              }}
            />
          </Fragment>
        )}
      </View>
      <Share coreText={mapLanguages(quote)[deviceLanguage]} shotRef={shotRef} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingRight: 15,
    paddingLeft: 15,
    paddingTop: 0,
    justifyContent: 'center',
  },
  font_serif: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 25,
    fontFamily: 'Tinos-Regular',
  },
  font_justme: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 30,
    fontFamily: 'JustMeAgainDownHere-Regular',
  },
  font_lobster: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 25,
    fontFamily: 'Lobster-Regular',
  },
  outerContainer: {
    flex: 1,
    backgroundColor: '#4215b3',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  backgroundVideo: {
    marginTop: 16,
    width: '80%',
    aspectRatio: 16.0 / 9.0,
  },
  quote: {
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;
