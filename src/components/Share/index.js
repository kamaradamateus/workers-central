import React from 'react';
import {Linking, View, StyleSheet} from 'react-native';
import {SocialIcon, Icon} from 'react-native-elements';
import {EXTRA_REGEX_REPLACE_4SHARE} from '../../config/config';
import {
  getTwitterURL,
  shareFacebook,
  shareInstagram,
  shareWhatsapp,
} from '../../constants/url';

export default Share = (props) => {
  const styles = StyleSheet.create({
    outer: {
      flexDirection: 'row',
    },
  });

  return (
    <View style={styles.outer}>
      <SocialIcon
        type="twitter"
        light
        onPress={() =>
          Linking.openURL(
            getTwitterURL(
              props.coreText
                .replace(/<[^>]*>?/gm, '')
                .replace(EXTRA_REGEX_REPLACE_4SHARE, ''),
            ),
          )
        }
      />

      <SocialIcon
        type="facebook"
        light
        onPress={() =>
          shareFacebook(
            props.coreText
              .replace(/<[^>]*>?|&[^>]*|>/gm, '')
              .replace(EXTRA_REGEX_REPLACE_4SHARE, ''),
          )
        }
      />

      <SocialIcon type="instagram" light onPress={() => shareInstagram()} />

      <Icon
        raised
        name="whatsapp"
        type="font-awesome"
        color="#128c7e"
        onPress={() =>
          shareWhatsapp(
            props.coreText
              .replace(/<[^>]*>?/gm, '')
              .replace(EXTRA_REGEX_REPLACE_4SHARE, ''),
          )
        }
      />
    </View>
  );
};
