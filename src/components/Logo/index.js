import React from 'react';
import {Image, View, Text, StyleSheet} from 'react-native';
import {
  getLanguage,
  getLocalisedAppFirstName,
  getLocalisedAppLastName,
} from '../../modules';
//const logoImage = require('../../assets/images/logo/hammer-35883.png');
const logoImage = require('../../assets/images/logo/helmet.png');

export default Logo = () => {
  const styles = StyleSheet.create({
    text: {
      textAlign: 'center',
      color: '#fff',
      fontSize: 18,
      fontFamily: 'Righteous-Regular',
    },
    outer: {
      width: 120,
      height: 120,
      borderRadius: 100 / 2,
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
    },
    logo: {
      marginLeft: 8,
      marginRight: 8,
      width: 30,
      height: 30,
    },
  });

  const deviceLanguage = getLanguage();

  return (
    <View style={styles.outer}>
      <Text style={styles.text}>
        {getLocalisedAppFirstName(deviceLanguage)}
      </Text>
      <Image style={styles.logo} source={logoImage} />
      <Text style={styles.text}>{getLocalisedAppLastName(deviceLanguage)}</Text>
    </View>
  );
};
