import {load} from 'js-yaml';
import {QUOTES_URL} from '../../config/config';
import {getParsedDate} from '../../utils/dates';

export async function updateQuote() {
  const today = getParsedDate();
  const responseSchedule = await fetch(QUOTES_URL, {
    method: 'GET',
    headers: {
      'Cache-Control': 'no-cache',
    },
  });
  const response = await responseSchedule.text();
  const schedule_wrapper = load(response, 'utf8');
  return schedule_wrapper[0][today];
}
