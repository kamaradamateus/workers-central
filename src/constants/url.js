import {ShareDialog} from 'react-native-fbsdk';
import Share from 'react-native-share';
import {captureScreen} from 'react-native-view-shot';
import {
  getAppLink,
  getLocalisedMessageSulfix,
  getShortLocalisedMessageSulfix,
  getShareText,
} from '../modules';

export const TWITTER = 'https://twitter.com/intent/tweet?text=';
export const FACEBOOK = 'https://www.facebook.com/sharer.php?quote=';

export function getTwitterURL(coreText) {
  return encodeURI(TWITTER + coreText + ' ' + getShortLocalisedMessageSulfix());
  //TODO add link to the app
}

export function shareFacebook(coreText) {
  const shareLinkContent = {
    contentType: 'link',
    contentUrl: getAppLink(),
    quote: coreText + ' - ' + getLocalisedMessageSulfix(),
  };
  ShareDialog.show(shareLinkContent);
  //TODO add link to the app
}

export function shareInstagram() {
  const shareOptions = {
    title: 'Share image to instastory',
    method: Share.InstagramStories.SHARE_BACKGROUND_IMAGE,
    backgroundImage: '',
    social: Share.Social.INSTAGRAM_STORIES,
    /**
     * method: Share.InstagramStories.SHARE_BACKGROUND_AND_STICKER_IMAGE,
    backgroundImage: 'http://urlto.png',
    stickerImage: 'data:image/png;base64,<imageInBase64>', //or you can use "data:" link
    backgroundBottomColor: '#fefefe',
    backgroundTopColor: '#906df4',
    attributionURL: 'http://deep-link-to-app', //in beta
    social: Share.Social.INSTAGRAM_STORIES
     */
  };
  shareImage(shareOptions);
}

export function shareWhatsapp(coreText) {
  const shareOptions = {
    title: getShareText(),
    message: coreText + ' - ' + getLocalisedMessageSulfix(),
    url: getAppLink(),
    social: Share.Social.WHATSAPP,
  };
  share(shareOptions);
}

/*export function shareTelegram(coreText){
    const shareOptions = {
        title: getShareText(),
        message: coreText + " - " + getLocalisedMessageSulfix(),
        url: getAppLink(),
        social: Share.Social.,
      };
    share(shareOptions);
};*/

export function shareImage(options) {
  captureScreen({
    // Either png or jpg (or webm Android Only), Defaults: png
    format: 'jpg',
    // Quality 0.0 - 1.0 (only available for jpg)
    quality: 0.8,
    result: 'base64',
  }).then(
    (uri) => {
      options.backgroundImage = 'data:image/jpg;base64,' + uri;
      share(options);
    },
    (error) => console.error('Oops, snapshot failed', error),
  );
}

export function share(options) {
  Share.shareSingle(options)
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      err && console.log(err);
    });
}
