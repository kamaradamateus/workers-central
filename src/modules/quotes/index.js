import React, {useState, useEffect, Fragment} from 'react';
import {updateQuote} from '../../services/quotes';

export function useQuote() {
  const [quote, setQuote] = useState([]);

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await updateQuote();
        setQuote(response);
      } catch (error) {
        console.log('error', error);
      }
    }
    fetchData();
  }, []);

  return {quote, updateQuote};
}
