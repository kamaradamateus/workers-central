import {NativeModules, Platform} from 'react-native';

//if app is multilanguage dont use it, comment
//const singleLanguage = null;
const singleLanguage = 'pt_BR';

export function getLocalisedAppName(lang) {
  const coreNames = {
    en: ['Morning', 'Worker'],
    es: ['Buenos Días', 'Trabajador'],
    pt: ['Bom Dia', 'Trabalhador'],
  };

  const names = mapLanguages(coreNames);

  return names[lang] || names.en;
}

export function getEmptyCoreNames() {
  return {
    en: ['', ''],
    es: ['', ''],
    pt: ['', ''],
  };
}

export function mapLanguages(coreNames) {
  if (typeof coreNames === 'undefined' || coreNames === null) {
    coreNames = getEmptyCoreNames();
  } else if (Array.isArray(coreNames) && coreNames.length === 0) {
    coreNames = getEmptyCoreNames();
  } else if (typeof coreNames === 'string' && coreNames.length === 0) {
    coreNames = getEmptyCoreNames();
  }
  return {
    en: coreNames.en,
    en_AU: coreNames.en,
    en_BZ: coreNames.en,
    en_CA: coreNames.en,
    en_IE: coreNames.en,
    en_JM: coreNames.en,
    en_NZ: coreNames.en,
    en_ZA: coreNames.en,
    en_TT: coreNames.en,
    en_GB: coreNames.en,
    en_US: coreNames.en,
    es_AR: coreNames.es,
    es_BO: coreNames.es,
    es_CL: coreNames.es,
    es_CO: coreNames.es,
    es_CR: coreNames.es,
    es_DO: coreNames.es,
    es_EC: coreNames.es,
    es_SV: coreNames.es,
    es_GT: coreNames.es,
    es_HN: coreNames.es,
    es_MX: coreNames.es,
    es_NI: coreNames.es,
    es_PA: coreNames.es,
    es_PY: coreNames.es,
    es_PE: coreNames.es,
    es_PR: coreNames.es,
    es: coreNames.es,
    es_UY: coreNames.es,
    es_VE: coreNames.es,
    pt: coreNames.pt,
    pt_BR: coreNames.pt,
  };
}

export function getLocalisedAppFirstName(lang) {
  const names = getLocalisedAppName(lang);

  return names[0];
}

export function getLocalisedAppLastName(lang) {
  const names = getLocalisedAppName(lang);

  return names[1];
}

export function getAppLink() {
  return Platform.OS === 'ios'
    ? 'https://apps.apple.com/br/app/google-earth/id293622097'
    : 'https://play.google.com/store/apps/details?id=com.google.earth';
  //TODO add real app links
}

export function getLocalisedMessageSulfix() {
  const coreNames = {
    en: 'Obtained at Morning Worker® App',
    es: 'Obtenido en la aplicación Buenos Días Trabajador®',
    pt: 'Obtido no applicativo Bom Dia Trabalhador®',
  };

  const lang = getLanguage();
  const names = mapLanguages(coreNames);

  return names[lang] || names.en;
}

export function getShortLocalisedMessageSulfix() {
  const coreNames = {
    en: 'via Morning Worker®',
    es: 'via Buenos Días Trabajador®',
    pt: 'via Bom Dia Trabalhador®',
  };

  const lang = getLanguage();
  const names = mapLanguages(coreNames);

  return names[lang] || names.en;
}

export function getShareImageText() {
  const coreNames = {
    en: 'Share image',
    es: 'Compartir imagen',
    pt: 'Compartilhar imagem',
  };

  const lang = getLanguage();
  const names = mapLanguages(coreNames);

  return names[lang] || names.en;
}

export function getShareText() {
  const coreNames = {
    en: 'Share via',
    es: 'Compartir via',
    pt: 'Compartilhar via',
  };

  const lang = getLanguage();
  const names = mapLanguages(coreNames);

  return names[lang] || names.en;
}

export function getLanguage() {
  if (singleLanguage !== null) {
    return singleLanguage;
  }
  /*return Platform.OS === 'ios'
    ? NativeModules.SettingsManager.settings.AppleLocale ||
        NativeModules.SettingsManager.settings.AppleLanguages[0] //iOS 13
    : NativeModules.I18nManager.localeIdentifier;*/
}
