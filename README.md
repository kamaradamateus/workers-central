# Workers Central
## Portuguese [pt-br] see English bellow
## Obs.
Só funciona no android por enquanto

## Preparação para desenvolvedor
1. Somente se você tiver Windows: Fazer download do arquivo .exe e installar cygwin
https://cygwin.com/install.html


2. Instalar nodeJS:
https://nodejs.org/pt-br/download/
Fazer download to instalador para seu Sistema Operacional e executar o instalador até o final

Windows:
3. Abra o programa cygwin

Linux e MAC:
3. Abra o programa Terminal

4. Faça um clone deste projeto no gitlab 


npm install

## Rodando
npx react-native run-android

## Assemblying and testing debug apk
react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res
cd android
./gradlew assembleDebug
apk can be found at android/app/build/outputs/apk/debug/


## Multilanguage support
jump to modules/index.js
set singleLanguage as the language you will offer in this app
otherwise you can remove it and it will take the language of the smartphone
be aware that only es/en/pt are currently mapped, if you wanna add different ones, map them properly in the same index file

## Server-Side yaml
Example with quotes, multiline, quote + video and emoticon:
```yaml
- schedule:
  "22/04/2021":
    en: "Fire in the wick"
    es: "Fuego en pavio"
    pt: "Fogo no pavio"
  "23/04/2021": 
    en: "Fire in the wick Fire in the wick Fire in the wick Fire in the wick Fire in the wick"
    es: "Fuego en pavio Fuego en pavio Fuego en pavio Fuego en pavio Fuego en pavio Fuego en pavio"
    pt: >-
      <p class='font_lobster quote'><span>Text Above Video &#9994;<br/></span><video controls 
       src="https://url-from-video/video_001.mp4" type="video/mp4">
       Video não suportado
      </video></p>
```
## [en] English
## Disclaimer
Works only on Android so far

## Preparation
npm install

## Running
npx react-native run-android

## Assemblying and testing debug apk
react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res
cd android
./gradlew assembleDebug
apk can be found at android/app/build/outputs/apk/debug/


## Multilanguage support
jump to modules/index.js
set singleLanguage as the language you will offer in this app
otherwise you can remove it and it will take the language of the smartphone
be aware that only es/en/pt are currently mapped, if you wanna add different ones, map them properly in the same index file

## Server-Side yaml
Example with quotes, multiline, quote + video and emoticon:
```yaml
- schedule:
  "22/04/2021":
    en: "Fire in the wick"
    es: "Fuego en pavio"
    pt: "Fogo no pavio"
  "23/04/2021": 
    en: "Fire in the wick Fire in the wick Fire in the wick Fire in the wick Fire in the wick"
    es: "Fuego en pavio Fuego en pavio Fuego en pavio Fuego en pavio Fuego en pavio Fuego en pavio"
    pt: >-
      <p class='font_lobster quote'><span>Text Above Video &#9994;<br/></span><video controls 
       src="https://url-from-video/video_001.mp4" type="video/mp4">
       Video não suportado
      </video></p>
```